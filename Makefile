.PHONY: all debug sae doc clean

all: sae

# debug: export CFLAGS=-g
debug: clean
debug: sae

sae:
	make -C src all
	gcc -g $$(find . -type f -iname *.o -print) -o sae

doc:
	doxygen

clean:
	rm -f sae
	rm -rf doc
	make -C src clean
