/**
 * \file inscrits.h
 * 
 * \brief fonctionnalités & menu des candidats 
 */

#ifndef INSCRITS_H
#define INSCRITS_H

#include <stdio.h>
#include <stdlib.h>

#include "../collections/llist.h"

#define CHAINE 31

/**
 * \brief représente un choix d'un candidat
 */
typedef struct {
	char ville[CHAINE];
	char departement[CHAINE];
	enum {
		DOSSIER_NON_TRAITE = 0,
		DOSSIER_ADMIS = 1,
		DOSSIER_REFUSE = -1,
		DOSSIER_LISTE_ATTENTE = 2
	} decisionDep;
	enum {
		DECISION_AUCUNE = 0,
		DECISION_REFUSE = -1,
		DECISION_ACCEPTE = 1
	} valCand;
} Choix;

#define FmtStatutDossier(v) (v == DOSSIER_NON_TRAITE ? "non traité" : (v == DOSSIER_ADMIS ? FmtASCIIColor(ASCII_COLOR_GREEN, "admis") : (v == DOSSIER_REFUSE ? FmtASCIIColor(ASCII_COLOR_RED, "refusé") : FmtASCIIColor(ASCII_COLOR_ORANGE, "sur liste d'attente"))))
#define FmtDecisionChoix(v) (v == DECISION_ACCEPTE ? FmtASCIIColor(ASCII_COLOR_GREEN, "accepté") : (v == DECISION_REFUSE ? FmtASCIIColor(ASCII_COLOR_RED, "refusé") : "aucune"))

/**
 * \brief représente un candidat
 */
typedef struct {
	int numCand;
	char nom[CHAINE];
	char prenom[CHAINE];
	
	struct {
		float math;
		float francais;
		float anglais;
		float specialite;
	} notes;
	
	LinkedList choix;
} Candidat;

/**
 * \brief libère de la mémoire un candidat
 * 
 * \param [in] candidat le candidat
 */
void free_candidat(Candidat* candidat);

/**
 * \brief charge depuis le disque les candidats
 * 
 * \param [in] nomFich le nom de fichier
 * 
 * \return la liste des candidats
 */
LinkedList charger_candidats(const char *nomFich);

/**
 * \brief sauvegarde sur le disque les candidats
 * 
 * \param [in] nomFich le nom de fichier
 * \param [in] list la liste des candidats
 */
void enregister_candidats(const char *nomFich, LinkedList *list);

/**
 * \brief recumère un candidat par son numéro
 * 
 * \param [in] candidats la liste des candidats
 * \param numCandidat le numéro du candidat
 * 
 * \return le pointeur vers le candidat ou NULL si pas trouvé 
 */
Candidat* get_candidat_by_num(LinkedList *candidats, int numCandidat);

/**
 * \brief trouve un choix d'un candidat en utilisant un itérateur
 * 
 * \param [in out] iter l'itérateur
 * \param [out] c le choix trouvé
 * \param [in] ville le nom de la ville du choix
 * \param [in] departement le nom du département du choix
 * 
 * \result le choix ou NULL si pas trouvé 
 */
Choix *trouver_choix_dans_iterateur(void *iter, Choix **c, const char *ville,
	const char *departement);

/**
 * \brief point d'entrée du menu des candidats
 */
void candidat(void* _param);

#endif
