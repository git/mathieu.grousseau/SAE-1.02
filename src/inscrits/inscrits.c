#include <string.h>
#include <errno.h>

#include "inscrits.h"
#include "../commun.h"
#include "../menu/menu.h"
#include "../IUT/IUT.h"

#define FSCANF_ERR_MSG(var) if (errno != 0) perror(__FILE__":"__LINE_STR__" ERREUR: fscanf "var); else fprintf(stderr, __FILE__":"__LINE_STR__" ERREUR: fscanf "var": format invalide\n");

/* récupère les inscrits dans un fichier texte et les enregistre dans un
tableau de type Candidat */
LinkedList charger_candidats(const char *const nomFich) {
	LinkedList candidats = llist_new();
	FILE *f;
	
	f = fopen(nomFich, "r");
	if (f == NULL) {
		perror("ERREUR: Impossible de charger la liste des candidats");
		/* exit(errno); */
		
		/* Il est probablement préférable de juste afficher l'erreur et de ne 
		pas planter le programme avec d'éventuelles données non sauvegardées */
		return candidats; /* Vide */
	}
	
	int nbInscrits;
	if (fscanf(f, "%d", &nbInscrits) != 1) {
		FSCANF_ERR_MSG("nbInscrits");
		
		/* un goto aurait pu êre justifié dans ces cas là... */
		fclose(f);	
		return candidats;
	}
	
	int i;
	for (i = 0; i < nbInscrits; i++) {
		Candidat *cand = (Candidat*) malloc(sizeof(Candidat));
		if (cand == NULL) {
			perror("liste candidats > candidat");
			fclose(f);
			return candidats;
		}
		
		/*partie scan de données*/
		if (fscanf(f, "%d%*c", &cand->numCand) != 1) {
			FSCANF_ERR_MSG("numCand");
			free(cand);
			fclose(f);
			return candidats;
		}
		
		if (!fget_str(cand->nom, f, CHAINE)) {
			free(cand);
			fclose(f);
			return candidats;
		}
		
		if (!fget_str(cand->prenom, f, CHAINE)) {
			free(cand);
			fclose(f);
			return candidats;
		}
		
		if (fscanf(f, "%f %f %f %f", &cand->notes.math, &cand->notes.francais, 
				&cand->notes.anglais, &cand->notes.specialite) != 4) {
			FSCANF_ERR_MSG("notes");
			free(cand);
			fclose(f);
			return candidats;
		}
		
		int nbChoix;
		if (fscanf(f, "%d%*c", &nbChoix) != 1) {
			FSCANF_ERR_MSG("nbChoix");
			free(cand);
			fclose(f);
			return candidats;
		}
		
		cand->choix = llist_new();
		llist_push_back(&candidats, cand);
		
		int j;
		for (j = 0; j < nbChoix; j++) {
			Choix *choix = (Choix*) malloc(sizeof(Choix));
			if (choix == NULL) {
				perror("candidat > liste choix");
				fclose(f);
				return candidats;
			}
			
			if (!fget_str(choix->ville, f, sizeof(choix->ville))) {
				free(choix);
				fclose(f);
				return candidats;
			}
			
			if (!fget_str(choix->departement, f, sizeof(choix->departement))) {
				free(choix);
				fclose(f);
				return candidats;
			}
			
			if (fscanf(f, "%d", &choix->decisionDep) != 1) {
				FSCANF_ERR_MSG("decisionDep");
				free(choix);
				fclose(f);
				return candidats;
			}
	 		if (fscanf(f, "%d%*c", &choix->valCand) != 1) {
				FSCANF_ERR_MSG("valCand");
				free(choix);
				fclose(f);
				return candidats;
			}
			
			llist_push_back(&cand->choix, choix);
		}
	}
	
	if (fclose(f) != 0) {
		perror("fclose");
	}
	
	return candidats;
}

void enregister_candidats(const char *nomFich, LinkedList *list) {
	/*
	/!\ Attention /!\
	
	Cette fonction est extremement sale.
	
	MAIS je voulais pas risquer ma vie pour avoir placé deux/trois goto...
	Et puis, c'est probablement un des rare cas ou le goto a quand même un 
	interret...
	(Dijkstra n'a jamais dit qu'il fallait banir le goto dailleurs...)
	
	https://www.cs.utexas.edu/~EWD/ewd13xx/EWD1308.PDF
	
	Bref...
	*/
	
	FILE *f = fopen(nomFich, "w");
	if (f == NULL) {
		perror("ERREUR: impossible d'enregistrer la liste des candidats");
		return;
	}
	
	if (!fprintf(f, "%d\n", (int) llist_len(list))) {
		fprintf(stderr, "Erreur écriture du fichier de candidats.\n");
		fclose(f);
		return;
	}
	
	void *iter = llist_iter_new(list);
	
	while (llist_iter_has_next(iter)) {
		Candidat *cand = (Candidat*) llist_iter_next(iter);
		
		if (!fprintf(f, "%d\n", cand->numCand)) {
			fprintf(stderr, "Erreur écriture du fichier de candidats.\n");
			llist_iter_destroy(iter);
			fclose(f);
			return;
		}
		
		if (!fprintf(f, "%s\n", cand->nom)) {
			fprintf(stderr, "Erreur écriture du fichier de candidats.\n");
			llist_iter_destroy(iter);
			fclose(f);
			return;
		}
		
		if (!fprintf(f, "%s\n", cand->prenom)) {
			fprintf(stderr, "Erreur écriture du fichier de candidats.\n");
			llist_iter_destroy(iter);
			fclose(f);
			return;
		}
		
		if (!fprintf(f, "%.2f %.2f %.2f %.2f\n", cand->notes.math, 
				cand->notes.francais, cand->notes.anglais,
				cand->notes.specialite)) {
			fprintf(stderr, "Erreur écriture du fichier de candidats.\n");
			llist_iter_destroy(iter);
			fclose(f);
			return;
		}
		
		if (!fprintf(f, "%d\n", (int) llist_len(&cand->choix))) {
			fprintf(stderr, "Erreur écriture du fichier de candidats.\n");
			llist_iter_destroy(iter);
			fclose(f);
			return;
		}
		
		void *iter2 = llist_iter_new(&cand->choix);
		
		while (llist_iter_has_next(iter2)) {
			Choix *choix = (Choix*) llist_iter_next(iter2);
			
			if (!fprintf(f, "%s\n", choix->ville)) {
				fprintf(stderr, "Erreur écriture du fichier de candidats.\n");
				llist_iter_destroy(iter2);
				llist_iter_destroy(iter);
				fclose(f);
				return;
			}
			
			if (!fprintf(f, "%s\n", choix->departement)) {
				fprintf(stderr, "Erreur écriture du fichier de candidats.\n");
				llist_iter_destroy(iter2);
				llist_iter_destroy(iter);
				fclose(f);
				return;
			}
			
			if (!fprintf(f, "%d\n", choix->decisionDep)) {
				fprintf(stderr, "Erreur écriture du fichier de candidats.\n");
				llist_iter_destroy(iter2);
				llist_iter_destroy(iter);
				fclose(f);
				return;
			}
			
			if (!fprintf(f, "%d\n", choix->valCand)) {
				fprintf(stderr, "Erreur écriture du fichier de candidats.\n");
				llist_iter_destroy(iter2);
				llist_iter_destroy(iter);
				fclose(f);
				return;
			}
		}
		
		llist_iter_destroy(iter2);
	}
	
	llist_iter_destroy(iter);
	
	printf("Sauvegarde du fichier candidat effectuée sans accroc\n");

	if (fclose(f) != 0) perror("fclose");
}

void free_candidat(Candidat* candidat) {
	if (candidat == NULL) return;
	
	llist_clear(&candidat->choix, (void (*)(void *)) free);
	
	free(candidat);
}

Candidat* get_candidat_by_num(LinkedList *candidats, int numCandidat) {
	void *iter = llist_iter_new(candidats);
	
	/* TODO: liste de candidats triée par numCandidat ? */
	while (llist_iter_has_next(iter)) {
		Candidat *c = (Candidat*) llist_iter_next(iter);
		
		if (c->numCand == numCandidat) {
			llist_iter_destroy(iter);
			return c;
		}
	}
	
	llist_iter_destroy(iter);
	return NULL;
}

Choix *trouver_choix_dans_iterateur(void *iter, Choix **c, const char *ville, const char *departement) {
	while (llist_iter_has_next(iter)) {
		*c = (Choix*) llist_iter_next(iter);
		
		if (!strcmp(departement, (*c)->departement) && !strcmp(ville, (*c)->ville))  {
			return *c;
		}
	}
	
	*c = NULL;
	return NULL;
}

typedef struct  {
	GenericSessionState generic;
	Candidat* candidat;
} CandidatStessionState;

void afficher_profil_candidat(void *param) {
	CandidatStessionState *session = (CandidatStessionState*) param;
	Candidat *c = session->candidat;
	
	printf(
		"Profil candidat\n===============\n\nNuméro de candidat:\t%d\nNom:\t%s\n"
		"Prenom:\t%s\nNotes:\n\tFrançais:\t%.1f\n\tAnglais:\t%.1f\n\t"
		"Mathématiques:\t%.1f\n\tSpécialité:\t%.1f\n\n",
		c->numCand,
		c->nom,
		c->prenom, 
		c->notes.francais, 
		c->notes.anglais,
		c->notes.math,
		c->notes.specialite
	);
}

void afficher_candidatures(void* param) {
	CandidatStessionState *session = (CandidatStessionState*) param;
	Candidat *c = session->candidat;
	
	printf("Liste des candidatures:\n");
	
	if (llist_len(&c->choix) == 0) {
		printf(FmtASCIIColor(ASCII_COLOR_ORANGE, "! Aucune candidature pour le moment !")"\n\n");
	} else {
		void* iter = llist_iter_new(&c->choix);
		
		printf(" %20s | %20s | %20s | %20s \n", "Département", "Ville", 
			"Accepation candidat", "Décision département");
		
		int i;
		for (i = 0; i < 4; i++) {
			int j;
			for (j = 0; j < 20 + 2; j++) putc('-', stdout);
			if (i < 3) putc('+', stdout);
		}
		putc('\n', stdout);
		fflush(stdout);
		
		while (llist_iter_has_next(iter)) {
			Choix *choix = (Choix*) llist_iter_next(iter);
			
			printf(" %20s | %20s | %20s | %20s \n",
				choix->departement, choix->ville,
				FmtDecisionChoix(choix->valCand),
				FmtStatutDossier(choix->decisionDep));
		}
		
		llist_iter_destroy(iter);
	}
}

void nouvelle_candidature(void *param) {
	CandidatStessionState *session = (CandidatStessionState*) param;
	if (session->generic.phase != PHASE_CANDIDATURES) {
		printf(FmtASCIIColor(ASCII_COLOR_ORANGE,
			"La phase de candidature est n'est pas activée")"\n");
		return;
	}
	
	Candidat *c = session->candidat;
	
	char ville[CHAINE];
	int indexVille;
	while (TRUE) {
		printf("Nom de la ville (vide pour annuler): ");	
		int r = read_line_opt(ville, sizeof_array(ville), stdin);
		if (r == EOF || !r) return;
		
		int trouve;
		indexVille = chercherVille(ville, session->generic.IUTs.nbVille,
			session->generic.IUTs.iut, &trouve);
		
		if (trouve) {
			break;
		}
		
		fprintf(stderr, "Il n'y a pas de ville portant ce nom.\n");
	}
	
	char departement[CHAINE];
	while (TRUE) {
		printf("Nom du département (vide pour annuler): ");
		int r = read_line_opt(departement, sizeof_array(departement), stdin);
		if (r == EOF || r) return;
		
		if (trouver_departement_par_nom(session->generic.IUTs.iut[indexVille], 
				departement) != NULL) {
			break;
		}
		
		fprintf(stderr, "Il n'y a pas de département portant ce nom.\n");
	}
	
	Choix *choix = (Choix*) malloc(sizeof(Choix));
	if (choix == NULL) {
		perror("ERREUR: echec d'allocation");
		return;
	}
	
	strcpy(choix->ville, ville);
	strcpy(choix->departement, departement);
	choix->decisionDep = DOSSIER_NON_TRAITE;
	choix->valCand = DECISION_AUCUNE;
	
	void *iter = llist_iter_new(&c->choix);
	
	Choix *choix2;
	while ((choix2 = (Choix*) llist_iter_peek(iter)) != NULL) {
		/* TODO Je trie par département puis par ville... l'inverse est mieux ? */
		int ord = strcmp(choix->departement, choix2->departement);
		if (ord == 0) ord = strcmp(choix->ville, choix2->ville);
		
		if (ord < 0) {
			break;
		} else if (ord == 0) {
			/* Wait, that's illegal */
			fprintf(stderr, "Il y a déjà une candidature dans ce département "
				"dans cette ville.\n");
			free(choix);
			llist_iter_destroy(iter);
			return;
		} else {
			llist_iter_next(iter);
			/* continue; */
		}
	}
	
	llist_iter_destroy(llist_iter_insert(iter, choix));
}

bool supprimer_candidature2(GenericSessionState *session, Candidat *c, const char *ville, const char *departement) {
	Choix *choix;
	
	void *iter = llist_iter_new(&c->choix);
	trouver_choix_dans_iterateur(iter, &choix, ville, departement);
	
	if (choix == NULL) {
		llist_iter_destroy(iter);
		return FALSE;
	}
	
	llist_iter_remove(iter);
	free(choix);
	llist_iter_destroy(iter);

	if (!strcmp(ville, "Clermont-Ferrand")
					&& !strcmp(departement, "Informatique")) {
		if (choix->decisionDep == DOSSIER_ADMIS) {
			iter = llist_iter_new(&session->listes.admis);
			while (llist_iter_has_next(iter)) {
				if (llist_iter_next(iter) == c) {
					llist_iter_remove(iter);
					break;
				}
			}
			llist_iter_destroy(iter);
			
			Candidat *nvadmis = llist_pop_front(&session->listes.attente);
			if (nvadmis != NULL) {
				Choix *choix2;
				
				iter = llist_iter_new(&nvadmis->choix);
				trouver_choix_dans_iterateur(iter, &choix2, "Clermont-Ferrand", "Informatique");
				llist_iter_destroy(iter);
				
				if (choix2 != NULL) {
					choix2->decisionDep = DOSSIER_ADMIS;
					llist_push_back(&session->listes.admis, nvadmis);
				}
			}
		} else if (choix->decisionDep == DOSSIER_LISTE_ATTENTE) {
			iter = llist_iter_new(&session->listes.attente);
			while (llist_iter_has_next(iter)) {
				if (llist_iter_next(iter) == c) {
					llist_iter_remove(iter);
					break;
				}
			}
			llist_iter_destroy(iter);
		}
	}
	
	return TRUE;
}

void supprimer_candidature(void *param) {
	CandidatStessionState *session = (CandidatStessionState*) param;
	if (session->generic.phase != PHASE_CANDIDATURES
			&& session->generic.phase != PHASE_ADMISSION) {
		printf(FmtASCIIColor(ASCII_COLOR_ORANGE,
			"La phase de candidature est n'est pas activée")"\n");
		return;
	}
	
	Candidat *c = session->candidat;
	
	char ville[CHAINE];
	while (TRUE) {
		printf("Nom de la ville (vide pour annuler): ");
		int r = read_line_opt(ville, sizeof_array(ville), stdin);
		if (r == EOF || !r) return;
		
		/* Cette vérif n'est techniquement pas nécéssaire */
		int trouve;
		chercherVille(ville, session->generic.IUTs.nbVille,
			session->generic.IUTs.iut, &trouve);
		
		if (trouve) {
			break;
		}
		
		fprintf(stderr, "Il n'y a pas de ville portant ce nom.\n");
	}
	
	char departement[CHAINE];
	while (TRUE) {
		printf("Nom du département (vide pour annuler): ");
		int r = read_line_opt(departement, sizeof_array(departement), stdin);
		if (r == EOF || !r) return;
		
		break;
	}
	
	if (!supprimer_candidature2(&session->generic, c, ville, departement)) {
		fprintf(stderr, "Il n'y a pas de candidature correspondante dans la liste.\n");
	}
}

void accepter_candidature(void *param) {
	CandidatStessionState *session = (CandidatStessionState*) param;

	if (session->generic.phase != PHASE_ADMISSION) {
		printf(FmtASCIIColor(ASCII_COLOR_ORANGE,
			"La phase d'acceptation est n'est pas disponible.")"\n");
		return;
	}
	
	Candidat *c = session->candidat;
	
	char ville[CHAINE];
	while (TRUE) {
		printf("Nom de la ville (vide pour annuler): ");
		int r = read_line_opt(ville, sizeof_array(ville), stdin);
		if (r == EOF || !r) return;
		
		/* Cette vérif n'est techniquement pas nécéssaire */
		int trouve;
		chercherVille(ville, session->generic.IUTs.nbVille,
			session->generic.IUTs.iut, &trouve);
		
		if (trouve) {
			break;
		}
		
		fprintf(stderr, "Il n'y a pas de ville portant ce nom.\n");
	}
	
	char departement[CHAINE];
	while (TRUE) {
		printf("Nom du département (vide pour annuler): ");
		int r = read_line_opt(departement, sizeof_array(departement), stdin);
		if (r == EOF || !r) return;
		
		break;
	}
	
	Choix *choix;
	
	void *iter = llist_iter_new(&c->choix);
	trouver_choix_dans_iterateur(iter, &choix, ville, departement) ;
	llist_iter_destroy(iter);
	
	if (choix == NULL) {
		fprintf(stderr, "Il n'y a pas de candidature correspondante dans la liste.\n");
		return;
	}
	
	if (choix->decisionDep != DOSSIER_ADMIS) {
		fprintf(stderr, "Cette candidature n'a pas encore été acceptée.\n");
		return;
	}
	
	while (TRUE) {
		printf("Etes vous sûr ? (OUI/non) ");
		char buf[3+1];				
		int r;
		if ((r = read_line_opt(buf, sizeof_array(buf), stdin)) == EOF) {
			break;
		} else if (r) {
			if (!strcmp(buf, "OUI")) {
				choix->valCand = DECISION_ACCEPTE;
				
				bool b;
				do {
					b = FALSE;
					iter = llist_iter_new(&c->choix);
					while (llist_iter_has_next(iter)) {
						Choix *choix2 = llist_iter_next(iter);
						
						if (choix2 != choix) {
							supprimer_candidature2(&session->generic, c,
								choix2->ville, choix2->departement);
							
							/* Astuce sale pour éviter les problèmes éventuels 
							de modifications concurrentes */
							b = TRUE;
							break;
						}
					}
					llist_iter_destroy(iter);
				} while (b);
			}
			
			break;
		}
	}
}

void candidat(void* _param) {
	CandidatStessionState session;
	init_generic_session_state(&session.generic);
	
	while (TRUE) {
		printf("Numéro de candidat (vide pour annuler): ");
		char buf[10];
		int r = read_line_opt(buf, sizeof_array(buf), stdin);
		if (r == EOF || !r) return;
		
		int numCandidat;
		if ((r = sscanf(buf, "%d", &numCandidat)) != 1) {
			if (r != EOF && r != 0) { FSCANF_ERR_MSG("numCandidat") };
			
			uninit_generic_session_state(&session.generic);
			return;
		}
		
		session.candidat = get_candidat_by_num(&session.generic.candidats, numCandidat);
		if (session.candidat == NULL) {
			fprintf(stderr, "Il n'y a pas de candidat avec ce numéro.\n");
		} else break;
	}
	
	printf(ASCII_RESET_TERM"\n");
	
	const OptionItem options[] = {
		{ "Information du profil", afficher_profil_candidat },
        { "Afficher les candidatures", afficher_candidatures },
		{ "Nouvelle candidature", nouvelle_candidature },
		{ "Supprimer une candidature", supprimer_candidature },
		{ "Accepter une candidature", accepter_candidature }
    };
    DOMENU(options, "Candidat>", &session);
	
	printf(ASCII_RESET_TERM"\n");
	uninit_generic_session_state(&session.generic);
}

