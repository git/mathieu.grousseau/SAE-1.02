/**
 * \file IUT.h
 * 
 * \brief fonctionnalités et menu en rapport avec les IUT 
 */

#ifndef IUT_H
#define IUT_H

#define CHAINE 31

#define MAX_CAPACITE_IUT 200

/**
 * \brief Un maillon de liste département
 */
typedef struct maillon MaillonDept;
/**
 * \brief Une liste chainée de départements
 */
typedef MaillonDept* ListeDept;

/**
 * \brief Un maillon de liste département
 */
struct maillon {
	char departement[CHAINE];
	/**
	 * \brief nombre de places
	 */
	int nbP;
	char responsable[CHAINE];
	ListeDept suiv;
};

/**
 * \brief Un maillon de liste de villes
 */
typedef struct villeiut {
	char ville[CHAINE];
	ListeDept ldept;
} VilleIUT;

/**
 * \brief recherche un maillon par son nom
 * 
 * \param [in] ville la ville dans laquelle chercher
 * \param [in] departement le nom de département
 * 
 * \return le maillon si trouvé ou NULL
 */
MaillonDept *trouver_departement_par_nom(VilleIUT *ville, char departement[CHAINE]);

/**
 * \brief charge les villes
 * 
 * \param [in] nomFich le nom de fichier
 * \param [out] nbVille le nombre de villes
 * \param [out] IUT les villes
 * 
 * \return les villes
 */
VilleIUT** charger_ville(const char * nomFich, int * nbVille, VilleIUT ** IUT);
/**
 * \brief sauvegarde les villes & départements
 * 
 * \param [in] nomFich le nom de fichier
 * \param [in] IUT les villes & départements
 * \param nbVille le nombre de départements
 */
void enregistrer_IUT(const char * nomFich, VilleIUT ** IUT, int nbVille);

#endif
