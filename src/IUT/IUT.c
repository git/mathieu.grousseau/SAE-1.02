#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "IUT.h"
#include "../commun.h"
#include "../collections/llist.h"

MaillonDept *trouver_departement_par_nom(VilleIUT *ville, char departement[CHAINE]) {
	MaillonDept *maillon = ville->ldept;
	
	while (maillon != NULL) {
		if (!strcmp(maillon->departement, departement)) break;
		maillon = maillon->suiv;
	}
	
	return maillon;
}

VilleIUT** charger_ville(const char * nomFich, int * nbVille, VilleIUT ** IUT) {
	char temp[CHAINE];
	int indiceIUT;

	
	FILE * f;

	f=fopen(nomFich, "r");
	if(f==NULL) {
		fprintf(stderr, "erreur d'ouverture de fichier\n");
	}

	*nbVille=0;
	int trouve;

	while(!feof(f)) {
		if (!fget_str(temp, f, CHAINE)) {
			if (ferror(f)) {
				fprintf(stderr, "Erreur lors du chargement du fichier IUT\n");
				fclose(f);
			} else {
				break;
			}
		}
		
		indiceIUT=chercherVille(temp, *nbVille, IUT, &trouve);

		if(trouve!=1) {
			VilleIUT * nouville=(VilleIUT *) malloc(sizeof(VilleIUT));
			strcpy(nouville->ville, temp);
			nouville->ldept = NULL;
			IUT[indiceIUT]=nouville;
			(*nbVille)++;
		}

		MaillonDept * nouveau=(MaillonDept*) malloc(sizeof(MaillonDept));

		if(!fget_str(nouveau->departement, f, CHAINE)) {
			fprintf(stderr, "Erreur lors du chargement du fichier IUT\n");
			free(nouveau);
			fclose(f);
			return IUT;
		}

		
		if(fscanf(f, "%d%*c", &nouveau->nbP) != 1) {
			fprintf(stderr, "Erreur lors du chargement du fichier IUT\n");
			free(nouveau);
			fclose(f);
			return IUT;
		}

		if(!fget_str(nouveau->responsable, f, CHAINE)) {
			fprintf(stderr, "Erreur lors du chargement du fichier IUT\n");
			free(nouveau);
			fclose(f);
			return IUT;
		}
		
		nouveau->suiv=NULL;

		if(IUT[indiceIUT]->ldept==NULL) {

			IUT[indiceIUT]->ldept=nouveau;
		}
		else {

			ListeDept actuel=IUT[indiceIUT]->ldept; 

			while(actuel->suiv!=NULL) {

				actuel=actuel->suiv;

			}

			actuel->suiv=nouveau;
		}
	}
	
	fclose(f);
	
	return IUT;
}



void enregistrer_IUT(const char * nomFich, VilleIUT ** IUT, int nbVille) {

	FILE *f = fopen(nomFich, "w");

	if(f == NULL) {
		fprintf(stderr, "Impossible d'enregistrer les IUT\n");
	}

	int i;

	for(i=0; i<nbVille ; i++) {

		if(IUT[i]->ldept==NULL) {

			if(!fprintf(f, "%s\n", IUT[i]->ville)) {
				fprintf(stderr, "erreur dans l'enregistrement des IUT\n");
			}
		}
		else {

			ListeDept actuel=IUT[i]->ldept;

			while(actuel!=NULL) {

				if(!fprintf(f, "%s\n", IUT[i]->ville)) {
					fprintf(stderr, "erreur dans l'enregistrement des IUT\n");
				}

				if(!fprintf(f, "%s\n", actuel->departement)) {
					fprintf(stderr, "erreur dans l'enregistrement des IUT\n");
				}

				if(!fprintf(f, "%d\n", actuel->nbP)) {
					fprintf(stderr, "erreur dans l'enregistrement des IUT\n");
				}

				if(!fprintf(f, "%s\n", actuel->responsable)) {
					fprintf(stderr, "erreur dans l'enregistrement des IUT\n");
				}

				actuel=actuel->suiv;

			}
		}

	}

printf("Sauvegarde du fichier IUT effectuée sans accroc.\n");

if(fclose(f) != 0) printf("Problème dans la fermeture du fichier : %s\n", nomFich);

}




