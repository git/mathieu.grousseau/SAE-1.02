#ifndef ADMISSION_H
#define ADMISSION_H

#include "../collections/llist.h"

LinkedList charger_liste_candidat(const char *nomFich, LinkedList *candidats);
void enregistrer_liste_candidat(const char *nomFich, LinkedList *candidats);

void admisseur(void* _param);

#endif
