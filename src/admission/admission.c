#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "admission.h"
#include "../commun.h"
#include "../menu/menu.h"
#include "../IUT/IUT.h"
#include "../inscrits/inscrits.h"

LinkedList charger_liste_candidat(const char *nomFich, LinkedList *candidats) {
    LinkedList list = llist_new();
    
    FILE *f = fopen(nomFich, "rb");
    if (f == NULL) {
        perror("chargement liste candidats");
        return list;
    }
    
    while (TRUE) {
        int r, numCand;
        if ((r = fread(&numCand, sizeof(numCand), 1, f)) != 1) {
            if (r == 0) break;
            
            fprintf(stderr, "ERREUR: chargement liste candidats: %s\n", feof(f) ? "identifiant incomplet (EOF)" : "erreur de lecture");
            fclose(f);
            return list;
        }
        
        Candidat *cand = get_candidat_by_num(candidats, numCand);;
        if (cand == NULL) {
            fprintf(stderr, "ATTENTION: numéro de candidat %d inconnu ignoré.\n", numCand);
        } else {
            llist_push_back(&list, cand);
        }
    }
    
    if (fclose(f) != 0) perror("fclose");
    
    return list;
}

void enregistrer_liste_candidat(const char *nomFich, LinkedList *candidats) {
    FILE *f = fopen(nomFich, "wb");
    if (f == NULL) {
        perror("sauvegarde liste candidats");
        return;
    }
    
    void *iter = llist_iter_new(candidats);
    
    while (llist_iter_has_next(iter)) {
        Candidat *cand = llist_iter_next(iter);
        
        if (fwrite(&cand->numCand, sizeof(int), 1, f) != 1) {
            fprintf(stderr, "ERREUR: écriture liste candidats");
            
            break;
        }
    }
    
    llist_iter_destroy(iter);
    if (fclose(f) != 0) perror("fclose");
}

typedef struct {
    GenericSessionState generic;
    VilleIUT *ville;
    MaillonDept *departement;
    struct {
        Candidat **cand;
        size_t len;
    } cached_alpha_sort;
} AdmisseurSessionState;

int alpha_cmp(const char *aa, const char *ab, const char *ba, const char *bb) {
    int ord = strcmp(aa, ba);
    if (!ord) ord = strcmp(ab, bb);
    return ord;
}

int alpha_cmp_candidat(Candidat *a, Candidat *b) {
    return alpha_cmp(a->prenom, a->nom, b->prenom, b->nom);
}

void quicksort_candidats(Candidat **tableau, size_t len) {
    size_t i, j;
    Candidat *w, *d;
    
    if (len <= 1) return;
    
    i = 0;
    j = len - 2;
    d = tableau[len - 1];
    
    while (i <= j) {
        while (alpha_cmp_candidat(tableau[i], d) <= 0 && i < (len - 1)) i++;
        while (alpha_cmp_candidat(tableau[j], d) >= 0 && 0 < j) j--;
        if (i < j) {
            w = tableau[i];
            tableau[i] = tableau[j];
            tableau[j] = w;
        }
    }
    
    tableau[len - 1] = tableau[i];
    tableau[i] = d;
    
    quicksort_candidats(tableau, i);
    quicksort_candidats(tableau + i + 1, len - i - 1);
}

Candidat **candidats_acceptes_tries(LinkedList *candidats, const char *ville, MaillonDept *departement, size_t *len) {
    *len = departement->nbP;
    Candidat **tableau = (Candidat**) malloc(sizeof(Candidat*) * (*len));
    if (tableau == NULL) return NULL;
    
    {
        size_t i = 0;
        
        void *iter = llist_iter_new(candidats);
        
        while (i < *len && llist_iter_has_next(iter)) {
            Candidat *c =  llist_iter_next(iter);
            
            void *iter2 = llist_iter_new(&c->choix);
            
            while (llist_iter_has_next(iter2)) {
                Choix *ch = llist_iter_next(iter2);
                
                int ord = strcmp(ch->ville, ville);
                if (!ord) ord = strcmp(ch->departement, departement->departement);
                if (!ord) {
                    if (ch->decisionDep == DOSSIER_ADMIS) tableau[i++] = c;
                    break;
                }
            }
            
            
            llist_iter_destroy(iter2);
        }
        
        llist_iter_destroy(iter);
        
        *len = i;
    }
    
    quicksort_candidats(tableau, *len);
    
    return tableau;
}

Candidat **candidats_acceptes_tries2(AdmisseurSessionState *session) {
    if (session->cached_alpha_sort.cand == NULL) {
        session->cached_alpha_sort.cand = candidats_acceptes_tries(
            &session->generic.candidats, "Clermont-Ferrand", 
            session->departement, &session->cached_alpha_sort.len);
    }
    
    return session->cached_alpha_sort.cand;
}

void afficher_statistiques(void *param) {
    AdmisseurSessionState *session = (AdmisseurSessionState*) param;
    
    printf("%d candidats admis\n%d candidats en attente\n\n", 
        (int) llist_len(&session->generic.listes.admis),
        (int) llist_len(&session->generic.listes.attente));
}

void afficher_candidats_acceptes(void *param) {
    AdmisseurSessionState *session = (AdmisseurSessionState*) param;
    
    printf("Candidats admis (dans l'ordre):\n");
    
    Candidat **candidats = candidats_acceptes_tries2(session);
    size_t i;
    for (i = 0; i < session->cached_alpha_sort.len; i++) {
        printf("%d\t%s %s\n", candidats[i]->numCand, candidats[i]->nom,
            candidats[i]->prenom);
    }
}

void afficher_candidats_attente(void *param) {
    AdmisseurSessionState *session = (AdmisseurSessionState*) param;
    
    printf("Candidats en attente (dans l'ordre):\n");
    
    void *iter = llist_iter_new(&session->generic.listes.attente);
    while (llist_iter_has_next(iter)) {
        Candidat *c = llist_iter_next(iter);
        printf("%d\t%s %s\n", c->numCand, c->nom, c->prenom);
    }
    llist_iter_destroy(iter);
}

void modifier_places(void *param) {
    AdmisseurSessionState *session = (AdmisseurSessionState*) param;
    
    if (session->generic.phase != PHASE_INSCRIPTIONS) {
        fprintf(stderr, "Il n'est plus possible de modifier le nombre de places.\n");
        return;
    }
    
    while (TRUE) {
        printf("Nouveau nombre de places (ancien: %d): ", session->departement->nbP);
        if (scanf("%d%*c", &session->departement->nbP) == 1) break;
        
        if (feof(stdin)) return;
        fprintf(stderr, "Nombre invalide.\n");
    }
}

struct candidat_note {
    Candidat *candidat;
    Choix *choix;
    float note;
};

void quicksort_notes(struct candidat_note *e, size_t len) {
    size_t i, j;
    struct candidat_note w, d;
    
    if (len <= 1) return;
    
    i = 0;
    j = len - 2;
    d = e[len - 1];
    
    while (i <= j) {
        while (e[i].note <= d.note && i < (len - 1)) i++;
        while (e[j].note >= d.note && 0 < j) j--;
        if (i < j) {
            w = e[i];
            e[i] = e[j];
            e[j] = w;
        }
    }
    
    e[len - 1] = e[i];
    e[i] = d;
    
    quicksort_notes(e, i);
    quicksort_notes(e + i + 1, len - i - 1);
}

void filtrer_notes(void *param) {
    AdmisseurSessionState *session = (AdmisseurSessionState*) param;
    
    struct {
		float math;
		float francais;
		float anglais;
		float specialite;
	} coefs;
    float min;
        
    printf("Coéficients de chaque matière:\n");
    printf("\tMathématiques: ");
    scanf("%f", &coefs.math);
    printf("\tFrançais: ");
    scanf("%f", &coefs.francais);
    printf("\tAnglais: ");
    scanf("%f", &coefs.anglais);
    printf("\tSpécialité: ");
    scanf("%f", &coefs.specialite);
    
    printf("\tNote minimale: ");
    scanf("%f%*c", &min);
    
    struct {
        struct candidat_note *ptr;
        size_t len, cap;
    } tableau = { NULL, 0, session->departement->nbP };
    
    tableau.ptr = (struct candidat_note*) malloc(sizeof(*tableau.ptr) * tableau.cap);
    if (tableau.ptr == NULL) {
        perror("malloc");
        return;
    }
    
    void *iter = llist_iter_new(&session->generic.candidats);;
    while (llist_iter_has_next(iter)) {
        Candidat *cand = llist_iter_next(iter);
        
        void *iter2 = llist_iter_new(&cand->choix);
        
        while (llist_iter_has_next(iter2)) {
            Choix *choix = llist_iter_next(iter2);
            
            if (!strcmp(choix->ville, "Clermont-Ferrand")
                    && !strcmp(choix->departement, "Informatique")) {
                float note = (
                    coefs.math * cand->notes.math
                    + coefs.francais * cand->notes.francais
                    + coefs.anglais * cand->notes.anglais
                    + coefs.specialite * cand->notes.specialite) / (
                        coefs.math
                        + coefs.francais
                        + coefs.anglais
                        + coefs.specialite);
                
                if (note < min) {
                    choix->decisionDep = DOSSIER_REFUSE;
                    continue;
                }
                
                if (tableau.len >= tableau.cap) {
                    tableau.cap *= 2;
                    tableau.ptr = (struct candidat_note*) realloc(tableau.ptr, 
                        sizeof(*tableau.ptr) * tableau.cap);
                    if (tableau.ptr == NULL) {
                        perror("realloc");
                        llist_iter_destroy(iter2);
                        llist_iter_destroy(iter);
                        return;
                    }
                }
                
                tableau.ptr[tableau.len++] = (struct candidat_note) {
                    cand, 
                    choix,
                    note
                };
                
                break;
            }
        }
        
        llist_iter_destroy(iter2);
    }
    llist_iter_destroy(iter);

    quicksort_notes(tableau.ptr, tableau.len);
    
    while (llist_len(&session->generic.listes.admis) < session->departement->nbP 
            && tableau.len>0) {
        llist_push_back(&session->generic.listes.admis, tableau.ptr[--tableau.len].candidat);
        tableau.ptr[tableau.len].choix->decisionDep = DOSSIER_ADMIS;
    }
    
    while (tableau.len>0) {
        llist_push_back(&session->generic.listes.attente,
            tableau.ptr[--tableau.len].candidat);
        tableau.ptr[tableau.len].choix->decisionDep = DOSSIER_LISTE_ATTENTE;
    }
    
    free(tableau.ptr);
    
    afficher_statistiques(param);
}

void chercher_candidat(void *param) {
    AdmisseurSessionState *session = (AdmisseurSessionState*) param;
    
    Candidat **candidats = candidats_acceptes_tries2(session);
    
    printf("Nom (vide pour annuler): ");
    char nom[CHAINE];
    int r;
    if ((r = read_line_opt(nom, sizeof_array(nom), stdin)) == EOF || !r) return;
    
    printf("Prenom (vide pour annuler): ");
    char prenom[CHAINE];
    if ((r = read_line_opt(prenom, sizeof_array(prenom), stdin)) == EOF || !r) return;
    
    { /* Recherche dichotomique */
        size_t debut = 0, fin = session->cached_alpha_sort.len, milieu;
        int diff;
        
        while (debut <= fin) {
            milieu = (debut + fin) / 2;
            
            Candidat *c = candidats[milieu];
            diff = alpha_cmp(c->prenom, c->nom, prenom, nom);
            
            if (!diff) {
                printf("%s %s (#%d) est admis dans ce département", nom, prenom,
                    c->numCand);
                Choix *choix;
                void *iter = llist_iter_new(&c->choix);
                trouver_choix_dans_iterateur(iter, &choix, session->ville->ville,
                    session->departement->departement);
                llist_iter_destroy(iter);
                
                if (choix == NULL || choix->valCand != DECISION_ACCEPTE) {
                    printf(" mais n'a pas validé ce choix.\n");
                } else {
                    printf(".\n");
                }
                
                return;
            } else if (diff < 0) debut = milieu + 1;
            else fin = milieu - 1;
        }
    }
    
   fprintf(stderr, "%s %s n'est pas dans la liste des admis.\n", nom, prenom); 
}

void admisseur(void *param) {
    AdmisseurSessionState session;
    init_generic_session_state(&session.generic);
    
    char ville[CHAINE];
    strcpy(ville, "Clermont-Ferrand");
    
    int trouve;
    int index = chercherVille(ville, session.generic.IUTs.nbVille,
        session.generic.IUTs.iut, &trouve);
    
    if (!trouve) {
        fprintf(stderr, "Il n'y a pas de ville portant ce nom.\n");
                
        init_generic_session_state(&session.generic);
        return;
    }
    
    char departement[CHAINE];
    strcpy(departement, "Informatique");
    
    session.ville = session.generic.IUTs.iut[index];    
    session.departement = trouver_departement_par_nom(session.ville, departement);
    
    if (session.departement == NULL) {
        fprintf(stderr, "Il n'y a pas de département portant ce nom.\n");
        
        init_generic_session_state(&session.generic);
        return;
    }
    
    session.cached_alpha_sort.cand = NULL;
    session.cached_alpha_sort.len = 0;
    
    printf(ASCII_RESET_TERM"\n");
    
    const OptionItem options[] = {
        { "Statistiques", afficher_statistiques },
        { "Afficher les candidats acceptés", afficher_candidats_acceptes },
        { "Afficher les candidats en attente", afficher_candidats_attente },
        { "Rechercher un candidat", chercher_candidat },
        { "Modifier le nombre de places du département", modifier_places },
        { "Filtrer par note", filtrer_notes },
    };
    DOMENU(options, "Admisseur", &session);
    
    printf(ASCII_RESET_TERM"\n");
    
    if (session.cached_alpha_sort.cand != NULL)
        free(session.cached_alpha_sort.cand);
    uninit_generic_session_state(&session.generic);
}
