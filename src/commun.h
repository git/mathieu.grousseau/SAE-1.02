/**
 * \file commun.h
 * 
 * \brief Contient des fonctionnalités communes a tout le projet.
 */

#ifndef COMMUN_H
#define COMMUN_H

#include <stdio.h>

#define sizeof_array(val) (sizeof(val) / sizeof(*val))

#define AS_STR2(x) #x
#define AS_STR(x) AS_STR2(x)
#define __LINE_STR__ AS_STR(__LINE__)

#define ASCII_RESET_TERM    "\e[1;1H\e[2J"
#define ASCII_COLOR_RESET   "\e[1;0m"
#define ASCII_COLOR_RED     "\e[1;31m"
#define ASCII_COLOR_GREEN   "\e[1;32m"
#define ASCII_COLOR_ORANGE  "\e[1;33m"
#define FmtASCIIColor(color, text)  color text ASCII_COLOR_RESET

/* TODO: utiliser stdbool.h (C99+) à la place ? */
/**
 * \brief un type booléen respectant les conventions booléennes du C 
 */
typedef enum bool {
    FALSE = 0,
    TRUE = 1
} bool;

/**
 * \brief tronque les retours à la ligne.
 * 
 * \param [in out] str le texte a tronquer
 * 
 * \return le même texte
 */
char *trim_newline(char *str);

/**
 * \brief teste si la ligne est vide
 * 
 * \param [in] str le texte a tester
 * 
 * \return un booléen vrai si vide 
 */
bool str_is_blank(const char *str);

/**
 * \brief lis une ligne et indique si la ligne est significative.
 * 
 * \param [out] dest le tampon a remplir
 * \param cap la capacité du tampon
 * \param FILE la source
 * 
 * \return EOF si la fin de fichier a été atteinte (ou erreur), 0 si non 
 *      sigificative.
 */
int read_line_opt(char *dest, int cap, FILE *f);

bool fget_str(char *dest, FILE *src, int cap);

#include "collections/llist.h"
#include "IUT/IUT.h"

/**
 * \brief Contient des données communes a toutes les 'séssions'.
 * 
 * Cette structure est trimballée dans casiment tous les menus. 
 */
typedef struct {
    LinkedList candidats;
    struct {
        int nbVille;
        VilleIUT *iut[MAX_CAPACITE_IUT];
    } IUTs;
    enum {
        PHASE_INSCRIPTIONS = 0,
        PHASE_CANDIDATURES = 1,
        PHASE_ADMISSION = 2
    } phase;
    struct {
        LinkedList attente;
        LinkedList admis;
    } listes;
} GenericSessionState;

/**
 * \brief Initialize la structure GenericSessionState
 * 
 * \param [out] session la structure a initialiser
 */
void init_generic_session_state(GenericSessionState *session);
/**
 * \brief Déinitialize la structure GenericSessionState
 * 
 * \param [in] session la structure a deinitialiser
*/
void uninit_generic_session_state(GenericSessionState *session);
/**
 * \brief Recherche une VilleIUT
 * 
 * \param [in] nomVille le nom de la ville
 * \param [in] IUT le tableau
 * \param nbVille la taille logique du tableau
 * \param [out] trouve si la ville a été trouvée
 * 
 * \return l'indice de la ville (ou sa position d'insertion si pas trouve)
*/
int chercherVille(char nomVille[], int nbVille, VilleIUT ** IUT, int * trouve);

#endif
