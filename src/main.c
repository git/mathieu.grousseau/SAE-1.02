#include <stdio.h>
#include <string.h>

#include "menu/administration.h"
#include "admission/admission.h"
#include "menu/menu.h"
#include "inscrits/inscrits.h"
#include "IUT/IUT.h"
#include "parcourir/parcourir.h"

int main(void) {
    const OptionItem options[] = {
        { "Administrateur", administrateur },
        { "Candidat", candidat },
        { "Parcourir les formations", parcourir },
        { "Admisseur", admisseur }
    };
    DOMENU(options, "Choissisez un profil", NULL);

    return 0;
}



/* #include <stdio.h>

int main(void)
{
    int i, j, n;

    for (i = 0; i < 11; i++) {
        for (j = 0; j < 10; j++) {
            n = 10 * i + j;
            if (n > 108) break;
            printf("\033[%dm %3d\033[m", n, n);
        }
        printf("\n");
    }
    return 0;
} */

/* int main(void) {
	char nomFich[100];
	int nbInscrits=0;
	strcpy(nomFich, "../donnees/candidats.txt");
	User * tablo;
	tablo=enregistrer(nomFich, &nbInscrits);
	afficherTout(tablo, nbInscrits);
	return 0;
} */