#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

#include "llist.h"

struct lnode {
    NodeLnk next;
    LL_VALUE value;
};

LinkedList llist_new(void) {
    return (LinkedList) { NULL, NULL, 0 };
}

size_t llist_len(const LinkedList *list) {
    return list->len;
}

LinkedList* llist_push_back(LinkedList *list, LL_VALUE value) {
    NodeLnk node = (NodeLnk) malloc(sizeof(struct lnode));
    if (node == NULL) {
        perror("llist_add: malloc");
        exit(errno);
    }
    
    node->next = NULL; /* On ajoute a la fin */
    node->value = value;
    
    if (list->len == 0) list->first = node;
    else list->last->next = node;
    
    list->last = node;
    list->len++;
    
    return list;
}

LL_VALUE llist_pop_front(LinkedList *list) {
    if (list->len == 0) return NULL;
    
    NodeLnk node = list->first;
    LL_VALUE value = node->value;
    
    list->first = node->next;
    if (--list->len == 0) list->last = NULL;
    
    free(node);
    
    return value;
}

LinkedList* llist_clear(LinkedList *list, void(consumer)(LL_VALUE)) {
    NodeLnk current = list->first;
    
    list->len = 0;
    list->first = list->last = NULL;
    
    while (current != NULL) {
        NodeLnk next = current->next;
        
        if (consumer != NULL) consumer(current->value);
        free(current);
        
        current = next;
    }
    
    return list;
}

typedef struct iterator {
    LinkedList *list;
    NodeLnk last, last2;
} * LLIterator;

void* llist_iter_new(LinkedList* list) {
    LLIterator iter = (LLIterator) malloc(sizeof(struct iterator));
    if (iter == NULL) {
        perror("llist_iter_new: malloc");
        exit(errno);
    }
    
    iter->list = list;
    iter->last = NULL; /* ist->first; */
    iter->last2 = NULL;
    
    return iter;
}

LL_VALUE llist_iter_next(void* iterator) {
    LLIterator iter = (LLIterator) iterator;
    
    if (!llist_iter_has_next(iter)) return NULL;
    
    LL_VALUE value = llist_iter_peek(iter);
    
    if (iter->last == NULL) {
        iter->last = iter->list->first;
    } else {
        iter->last2 = iter->last;
        iter->last = iter->last->next;
    }
    
    return value;
}

LL_VALUE llist_iter_peek(void* iterator) {
    LLIterator iter = (LLIterator) iterator;
    
    if (!llist_iter_has_next(iter)) return NULL;
    
    if (iter->last == NULL) {
        return iter->list->first->value;
    } else {
        return iter->last->next->value;
    }
}

bool llist_iter_has_next(void* iterator) {
    LLIterator iter = (LLIterator) iterator;
    
    if (iter->last == NULL) return iter->list->len > 0;
    else if (iter->last->next == NULL) return FALSE;
    else return TRUE;
}

void* llist_iter_insert(void* iterator, LL_VALUE value) {
    LLIterator iter = (LLIterator) iterator;
    
    NodeLnk node = (NodeLnk) malloc(sizeof(struct lnode));
    if (node == NULL) {
        perror("llist_iter_insert: malloc");
        exit(errno);
    }
    
    node->value = value;
    if (iter->last == NULL) {
        node->next = iter->list->first;
    } else {
        node->next = iter->last;
    }
    
    if (iter->last2 == NULL) {
        /* Si last2 est NULL alors on ajoute à la position du 1er element */
        iter->list->first = node;
    } else {
        iter->last2->next = node;
    }
    
    iter->list->len++;
    
    return iter;
}

LL_VALUE llist_iter_remove(void *iterator) {
    LLIterator iter = (LLIterator) iterator;
    
    if (iter->last == NULL) {
        fprintf(stderr, "Erreur: bug -> llist_iter_remove: l'iterateur n'a \
retourne aucun element\n");
        exit(1);
    }
    
    NodeLnk removed = iter->last;
    
    if (iter->last2 == NULL) {
        /* 
         * le dernier element retrouné était le 1er element de la liste, donc si
         * on le retire, on change aussi la tête de liste
         */
        iter->list->first = removed->next;
    } else {
        iter->last2->next = removed->next;
    }
    
    if (removed->next == NULL) iter->list->last = iter->last2;
    
    iter->list->len--;
    
    LL_VALUE value = removed->value;
    free(removed);
    
    return value;
}

void llist_iter_destroy(void* iterator) {
    free(iterator);
}
