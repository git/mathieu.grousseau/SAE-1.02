/**
 * \file llist.h
 * \brief Liste et File chainée
 */

#ifndef LLIST_H
#define LLIST_H
#include <stddef.h>

typedef void* LL_VALUE;

typedef struct lnode* NodeLnk;
typedef struct llist {
    NodeLnk first, last;
    size_t len;
} LinkedList;

#include "../commun.h"

/**
 * \brief Crée une liste/file chainée vide
 * \return La liste, **pas** derrière un pointeur
 */
LinkedList llist_new(void);

/**
 * \param [in] list un pointeur constant vers la liste
 * \return La longueur de la liste/file
 */
size_t llist_len(const LinkedList *list);

/**
 * \brief Ajoute a la fin de la liste/file l'element donné
 * 
 * \param [in] list un pointeur vers la liste non NULL
 * \param [in] value la valeur a jouter, la valeur NULL est autorisée
 * 
 * \return la liste/file
 */
LinkedList* llist_push_back(LinkedList *list, LL_VALUE value);

/**
 * \brief Retire le premier element de la liste/file.
 * 
 * \param [in out] list un pointeur vers la liste non NULL
 * 
 * \return l'élement retité ou NULL si la collection est vide 
 */
LL_VALUE llist_pop_front(LinkedList *list);

/**
 * 
 */
LinkedList* llist_clear(LinkedList *list, void(consumer)(LL_VALUE));

/**
 * \brief Crée un itérateur sur cette liste/file
 * 
 * \param [in] list un pointeur vers la liste non NULL
 * \return un itérateur
 * \details L'itérateur délivera les elements de la liste/file en commencant 
 * par le premier élement/élement en tête
 */
void* llist_iter_new(LinkedList *list);

/**
 * \brief Retourne le prochain element de l'itérateur
 * 
 * \param iterator un itérateur
 * 
 * \return la valeur ou NULL si la fin est atteinte
 */
LL_VALUE llist_iter_next(void* iterator);

/**
 * \brief Récupère le prochain élement mais n'avance pas l'itérateur
 * 
 * \param [in] iterator un itérateur
 * 
 * \return la valeur ou NULL si la fin est atteinte
 */
LL_VALUE llist_iter_peek(void* iterator);

/**
 * \brief Teste si il y a encore des elements disponibles
 * 
 * \param [in] iterator un itérateur
 */
bool llist_iter_has_next(void *iterator);

/**
 * \brief Insère a la position de la dernière valeur retournée.
 * \note Le prochain element retourné sera a nouveau l'ancien element retourné.
 * 
 * \param [in] iterator un itérateur
 * \param [in] value La valeur a insérer
 * 
 * \return l'itérateur
 */
void* llist_iter_insert(void *iterator, LL_VALUE value);

/**
 * \brief Retire le dernier element retourné
 * 
 * \param [in] iterator un itérateur
 * 
 * \return la valeur ou NULL si la fin est atteinte
 */
LL_VALUE llist_iter_remove(void *iterator);

/**
 * \brief Détruit un itérateur et libère les ressources associées
 * 
 * \param iterator un itérateur 
 */
void llist_iter_destroy(void* iterator);

#endif
