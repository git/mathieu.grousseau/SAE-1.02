/**
 * \file menu.h
 * 
 * \brief Contient le squelette de tous les menu du projet.
 */

#ifndef MENU_H
#define MENU_H

#include <stdlib.h>

#include "../commun.h"

/**
 * \brief Une option, avec son texte et la fonction associée.
 */ 
typedef struct {
    char* text;
    void(*actionfn)(void* param);
} OptionItem;

/**
 * \brief Affiche un menu a l'utilisateur
 * 
 * \param [in] options Le tableau des options
 * \param option_count le nombre d'options
 * \param [in out] param Un pointeur a donner aux fonctions des options, peut être NULL.
 */
void domenu(const OptionItem options[], size_t option_count,
    const char* const title, void* param);

#define DOMENU(menu, title, param) domenu(menu, sizeof(menu) / sizeof(OptionItem), title, param);

#endif
