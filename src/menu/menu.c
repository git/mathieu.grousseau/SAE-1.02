#include "menu.h"

#include <stdio.h>

void domenu(const OptionItem options[], size_t option_count, const char* const title, void* param) {
    size_t i;
    
    while (TRUE) {
        if (title != NULL) printf("%s :\n", title);
        
        for (i = 0; i < option_count; i++) {
            printf("%u.\t%s\n", (unsigned int) i, options[i].text);
        }
        printf("%u.\tRetour\n\n> ", (unsigned int) i);
        
        /* Isoler la lecture sur une ligne (en consommant \n) pour pas mettre le 
        bazar dans le reste du code*/
        char buf[20];
        if (fgets(buf, sizeof(buf) / sizeof(char), stdin) == NULL) return;
        
        
        unsigned int choice;
        int readen = sscanf(buf, "%u\n", &choice);
        if (readen != 1) {
            if (readen != EOF) fprintf(stderr, "Option invalide.\n");
            continue;
        }
        
        if (choice < option_count) {
            options[choice].actionfn(param);
            
            if (feof(stdin)) return;
        } else if (choice == option_count) {
            return; /* Retour */
        } else {
            fprintf(stderr, "Erreur: Option %u inconnue.\n", choice);
        }
    }
}
