#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "../commun.h"
#include "administration.h"
#include "menu.h"

void creer_departement(void *param) {

    GenericSessionState * session = (GenericSessionState*) param;
	int indiceIUT;

    char ville[CHAINE];
	while (TRUE) {
		printf("Nom de la ville (vide pour annuler): ");
		int r = read_line_opt(ville, sizeof_array(ville), stdin);
		if (r == EOF || !r) return;
		
		/* Cette vérif n'est techniquement pas nécéssaire */
		int trouve;
		indiceIUT=chercherVille(ville, session->IUTs.nbVille,
			session->IUTs.iut, &trouve);
		
		if (trouve) {
			break;
		}
		
		fprintf(stderr, "Il n'y a pas de ville portant ce nom.\n");
	}
    
    
    VilleIUT * nouville=(VilleIUT *) malloc(sizeof(VilleIUT));
    strcpy(nouville->ville, ville);
    session->IUTs.iut[indiceIUT]=nouville;
    (session->IUTs.nbVille)++;
    
    MaillonDept * nouveau=(MaillonDept*) malloc(sizeof(MaillonDept));

    char departement[CHAINE];

	printf("Nom du département (vide pour annuler): ");
	int r = read_line_opt(departement, sizeof_array(departement), stdin);
	if (r == EOF || !r) {
		free(nouveau);
		free(nouville);
		return;
	}

    strcpy(nouveau->departement, departement);

    int nbP;

	printf("Nombre de places dans le département : ");
	scanf("%d", &nbP);

    nouveau->nbP=nbP;

    char responsable[CHAINE];

	printf("Nom du responsable du département (vide pour annuler): ");
	r = read_line_opt(responsable, sizeof_array(responsable), stdin);
	if (r == EOF || !r) {
		free(nouveau);
		free(nouville);
		return;
	}

    strcpy(nouveau->responsable, responsable);
    
    nouveau->suiv=NULL;

    if(session->IUTs.iut[indiceIUT]->ldept==NULL) {
        session->IUTs.iut[indiceIUT]->ldept=nouveau;
    }
    else {
        ListeDept actuel=session->IUTs.iut[indiceIUT]->ldept; 
        while(actuel->suiv!=NULL) {
           actuel=actuel->suiv;
        }
        actuel->suiv=nouveau;
    }
}

void supprimer_departement(void * param) {
    
    GenericSessionState * session = (GenericSessionState*) param;

	int indiceIUT;

    char ville[CHAINE];
	while (TRUE) {
		printf("Nom de la ville (vide pour annuler): ");
		int r = read_line_opt(ville, sizeof_array(ville), stdin);
		if (r == EOF || !r) return;
		
		/* Cette vérif n'est techniquement pas nécéssaire */
		int trouve;
		indiceIUT=chercherVille(ville, session->IUTs.nbVille,
			session->IUTs.iut, &trouve);
		
		if (trouve) {
			break;
		}
		
		fprintf(stderr, "Cette ville ne contient pas d'IUT.\n");
	}

    char departement[CHAINE];

	printf("Nom du département (vide pour annuler): ");
	int r = read_line_opt(departement, sizeof_array(departement), stdin);
	if (r == EOF || !r) return;

    ListeDept actuel=session->IUTs.iut[indiceIUT]->ldept; 
    while(actuel->suiv!=NULL && strcmp(actuel->suiv->departement, departement)==0) {
       actuel=actuel->suiv;
    }
    if(strcmp(actuel->suiv->departement, departement)==0) {
        ListeDept temp=actuel->suiv->suiv;
        free(actuel->suiv);
        actuel->suiv=temp;
        return;
    }
    fprintf(stderr, "L'IUT de cette ville ne possède pas ce département !\n");
    return;
}

void modifier_places_departement(void * param) {
    
    GenericSessionState * session = (GenericSessionState*) param;
	int indiceIUT;

    char ville[CHAINE];
	while (TRUE) {
		printf("Nom de la ville (vide pour annuler): ");
		int r = read_line_opt(ville, sizeof_array(ville), stdin);
		if (r == EOF || !r) return;
		
		/* Cette vérif n'est techniquement pas nécéssaire */
		int trouve;
		indiceIUT=chercherVille(ville, session->IUTs.nbVille,
			session->IUTs.iut, &trouve);
		
		if (trouve) {
			break;
		}
		
		fprintf(stderr, "Cette ville ne contient pas d'IUT.\n");
	}

    char departement[CHAINE];

	printf("Nom du département (vide pour annuler): ");
	int r = read_line_opt(departement, sizeof_array(departement), stdin);
	if (r == EOF || !r) return;

    int nbP;

    printf("Nouveau nombre de places dans le département.\n");
    scanf("%d", &nbP);

    ListeDept actuel=session->IUTs.iut[indiceIUT]->ldept; 
    while(actuel->suiv!=NULL && strcmp(actuel->suiv->departement, departement)==0) {
       actuel=actuel->suiv;
    }

    if(strcmp(actuel->departement, departement)==0) {
        actuel->nbP=nbP;
        return;
    }
    fprintf(stderr, "L'IUT de cette ville ne possède pas ce département !\n");
    return;
}

void changer_responsable(void * param) {
    
    GenericSessionState * session = (GenericSessionState*) param;

	int indiceIUT;

    char ville[CHAINE];
	while (TRUE) {
		printf("Nom de la ville (vide pour annuler): ");
		int r = read_line_opt(ville, sizeof_array(ville), stdin);
		if (r == EOF || !r) return;
		
		/* Cette vérif n'est techniquement pas nécéssaire */
		int trouve;
		indiceIUT=chercherVille(ville, session->IUTs.nbVille,
			session->IUTs.iut, &trouve);
		
		if (trouve) {
			break;
		}
		
		fprintf(stderr, "Cette ville ne contient pas d'IUT.\n");
	}

    char departement[CHAINE];

	printf("Nom du département (vide pour annuler): ");
	int r = read_line_opt(departement, sizeof_array(departement), stdin);
	if (r == EOF || !r) return;

    char responsable[CHAINE];

	printf("Nom du nouveau reponsable (vide pour annuler): ");
	r = read_line_opt(responsable, sizeof_array(responsable), stdin);
	if (r == EOF || !r) return;

    ListeDept actuel=session->IUTs.iut[indiceIUT]->ldept; 
    while(actuel->suiv!=NULL && strcmp(actuel->departement, departement)==0) {
       actuel=actuel->suiv;
    }
    if(strcmp(actuel->departement, departement)==0) {
        strcpy(actuel->responsable, responsable);
        return;
    }
    fprintf(stderr, "L'IUT de cette ville ne possède pas ce département !\n");
    return;
}

void demarrer_phase_candidature(void *param) {
    GenericSessionState *session = (GenericSessionState*) param;
    session->phase = PHASE_CANDIDATURES;
}

void arreter_phase_candidature(void *param) {
    GenericSessionState *session = (GenericSessionState*) param;
    session->phase = PHASE_ADMISSION;
}

void administrateur(void* _param) {
    GenericSessionState session;
    init_generic_session_state(&session);
    
    printf(ASCII_RESET_TERM"\n");
    
    const OptionItem options[] = {
        { "Créer un département", creer_departement },
        { "Supprimer un département", supprimer_departement },
        { "Modifier le nombre de places d'un département", modifier_places_departement },
        { "Changer le responsable d'un département", changer_responsable },
        { "Démarrer la phase de candidature", demarrer_phase_candidature },
        { "Arrêter la phase de candidature", arreter_phase_candidature }
    };
    DOMENU(options, "Administration>", &session);

    printf(ASCII_RESET_TERM"\n");    
    uninit_generic_session_state(&session);
}


