/**
 * \file administration.h
 * 
 * \brief Contient les fonctionnalités du menu administrateur
 */

#ifndef ADMINISTRATION_H
#define ADMINISTRATION_H

/**
 * \brief point d'entrée du menu administrateur
 */
void administrateur(void* _param);

#endif
