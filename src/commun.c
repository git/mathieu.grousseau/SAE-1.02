#include <stdlib.h>
#include <string.h>

#include "commun.h"

#include "inscrits/inscrits.h"
#include "admission/admission.h"

#define DATA_DIRECTORY "donnees/"
const char* FILENAME_CANDIDATS = DATA_DIRECTORY"candidats.txt";
const char* FILENAME_VILLEIUT = DATA_DIRECTORY"IUT.txt";

char *trim_newline(char *str) {
    char* ptr = strchr(str, '\n');
    if (ptr != NULL) *ptr = '\0';
    
    return str;
}

bool str_is_blank(const char *str) {
    if (str == NULL) return TRUE;
    
    char c;
    while ((c = *str) != '\0') {
        if (c != ' ' && c != '\t' && c != '\n' && c != '\r') return FALSE;
    }
    
    return TRUE;
}

int read_line_opt(char *dest, int cap, FILE *f) {
    if (fgets(dest, cap, f) == NULL) return EOF;
    
    return str_is_blank(trim_newline(dest)) ? 0 : 1;
}

bool fget_str(char *dest, FILE *src, int cap) {
	int r = read_line_opt(dest, cap, src);
	if (r == EOF) {
		/* fprintf(stderr, "ERREUR: Erreur de lecture ou fin de fichier prématurée\n"); */
		return FALSE;
	} else if (r) return TRUE;
	
	fprintf(stderr, "ERREUR: Format invalide\n");
	
	return FALSE;
}

void init_generic_session_state(GenericSessionState *session) {
    session->phase = PHASE_CANDIDATURES;
    {
        FILE *f = fopen(DATA_DIRECTORY"etat.txt", "r");
        if (f == NULL) {
            perror("données d'état global");
        } else {
            int n;
            fscanf(f, "%d", &n);
            switch (n)
            {
            case PHASE_CANDIDATURES:
                session->phase = PHASE_CANDIDATURES;
                break;
                
            case PHASE_ADMISSION:
                session->phase = PHASE_ADMISSION;
                break;;
            
            default:
                session->phase = PHASE_INSCRIPTIONS;
                break;
            }
        }
        
        if (fclose(f) != 0) perror("fclose");
    }
    
    charger_ville(FILENAME_VILLEIUT, &session->IUTs.nbVille, session->IUTs.iut);
    session->candidats = charger_candidats(FILENAME_CANDIDATS);
    session->listes.admis = charger_liste_candidat(DATA_DIRECTORY"listes/admis", 
        &session->candidats);
    session->listes.attente = charger_liste_candidat(
        DATA_DIRECTORY"listes/attente", &session->candidats);
}

void uninit_generic_session_state(GenericSessionState *session) {
    {
        FILE *f = fopen(DATA_DIRECTORY"etat.txt", "w");
        if (f == NULL) {
            perror("données d'état global");
        } else {
            fprintf(f, "%d", session->phase);
        }
        
        if (fclose(f) != 0) perror("fclose");
    }
    
    enregister_candidats(FILENAME_CANDIDATS, &session->candidats);
    enregistrer_IUT(FILENAME_VILLEIUT, session->IUTs.iut, session->IUTs.nbVille);
    enregistrer_liste_candidat(DATA_DIRECTORY"listes/admis",
        &session->listes.admis);
    enregistrer_liste_candidat(DATA_DIRECTORY"listes/attente",
        &session->listes.attente);
    
    llist_clear(&session->candidats, (void(*)(void *)) free_candidat);
    
    llist_clear(&session->listes.admis, NULL);
    llist_clear(&session->listes.attente, NULL);
}

int chercherVille(char nomVille[], int nbVille, VilleIUT ** IUT, int * trouve) {
    int i;
    for(i=0;i<nbVille;i++){
        if(strcmp(nomVille, IUT[i]->ville)==0) {
            *trouve=1;
            return i;
        }
    }
    *trouve=0;
    return nbVille;
}
