/**
 * \file parcourir.h
 * 
 * \brief Regroupe les fonctions de menu pour parcourir les IUT.
 */

#ifndef PARCOURIR_H
#define PARCOURIR_H

#include "../IUT/IUT.h"

/**
 * \brief point d'entrée du menu de parcours des IUT
 */
void parcourir(void *_param);
/**
 * \param [in] param paramètre générique de session
 */
void afficherToutIUT(void * param);
/**
 * \param [in] param paramètre générique de session
 */
void afficherIUTVille(void * param);
/**
 * \param [in] param paramètre générique de session
 */
void afficherDepartementIUT(void * param);
/**
 * \param [in] param paramètre générique de session
 */
void afficherVilleIUT(void * param);
/**
 * \param [in] param paramètre générique de session
 */
void trouverDepartement(void * param);

#endif