#include <stdio.h>
#include <string.h>

#include "parcourir.h"
#include "../menu/menu.h"
#include "../IUT/IUT.h"
#include "../commun.h"

void parcourir(void *_param) {
	GenericSessionState session;
	init_generic_session_state(&session);

	const OptionItem options[] = {
		{ "Afficher tous les IUT", afficherToutIUT },
		{ "Rechercher les IUT d'une ville", afficherIUTVille },
		{ "Afficher un département spécifique à un IUT spécifique", afficherDepartementIUT},
		{ "Afficher tous les villes qui possèdent un IUT", afficherVilleIUT},
		{ "Afficher les IUT possédant un département spécifique", trouverDepartement},
    };
    DOMENU(options, "Candidat>", &session);
}

/*Affiche tous les IUT de la base de données*/

void afficherToutIUT(void * param) {

	GenericSessionState * session = (GenericSessionState*) param;

	int i;
	for(i=0; i<session->IUTs.nbVille; i++) {
		ListeDept temp=session->IUTs.iut[i]->ldept;
		printf("\n\n");
		printf("Ville : ");
		puts(session->IUTs.iut[i]->ville);
		while(temp != NULL) {
			printf("\n\n");
			printf("Département : ");
			puts(temp->departement);
			printf("Nombre de places : ");
			printf("%d\n", temp->nbP);
			printf("Nom du responsable : ");
			puts(temp->responsable);
			temp=temp->suiv;
			printf("\n\n");
		}
	}
}

/*Affiche l'ensemble des départements de l'IUT d'une ville donnée en par l'utilisateur*/

void afficherIUTVille(void * param) {

	GenericSessionState * session = (GenericSessionState*) param;

	int indiceIUT;
	int trouve;

	char ville[CHAINE];
	while (TRUE) {
		printf("Nom de la ville (vide pour annuler): ");
		int r = read_line_opt(ville, sizeof_array(ville), stdin);
		if (r == EOF || !r) return;

		/* Cette vérif n'est techniquement pas nécéssaire */
		indiceIUT=chercherVille(ville, session->IUTs.nbVille,
			session->IUTs.iut, &trouve);

		if (trouve) {
			break;
		}

		fprintf(stderr, "Cette ville ne contient pas d'IUT.\n");
	}

	ListeDept temp=session->IUTs.iut[indiceIUT]->ldept;
	printf("\n\n");
	printf("Ville : ");
	puts(session->IUTs.iut[indiceIUT]->ville);
	while(temp != NULL) {
		printf("\n\n");
		printf("Département : ");
		puts(temp->departement);
		printf("Nombre de places : ");
		printf("%d\n", temp->nbP);
		printf("Nom du responsable : ");
		puts(temp->responsable);
		temp=temp->suiv;
		printf("\n\n");
	}
}

/*Affiche un département spécifique dans un IUT spécifique*/

void afficherDepartementIUT(void * param) {

	GenericSessionState * session = (GenericSessionState*) param;

	int indiceIUT;

    char ville[CHAINE];
	while (TRUE) {
		printf("Nom de la ville (vide pour annuler): ");
		int r = read_line_opt(ville, sizeof_array(ville), stdin);
		if (r == EOF || !r) return;

		/* Cette vérif n'est techniquement pas nécéssaire */
		int trouve;
		indiceIUT=chercherVille(ville, session->IUTs.nbVille,
			session->IUTs.iut, &trouve);

		if (trouve) {
			break;
		}

		fprintf(stderr, "Cette ville ne contient pas d'IUT.\n");
	}

    char departement[CHAINE];

	printf("Nom du département (vide pour annuler): ");
	int r = read_line_opt(departement, sizeof_array(departement), stdin);
	if (r == EOF || !r) return;

	ListeDept temp=session->IUTs.iut[indiceIUT]->ldept;

	while(temp != NULL) {
		if(strcmp(temp->departement, departement)==0) {
			printf("\n\n");
			printf("Nombre de places : %d\n", temp->nbP);
			printf("Nom du responsable : %s\n", temp->responsable);
			printf("\n");
			return;
		}
		temp=temp->suiv;
	}
	fprintf(stderr, "Il n'y a pas de département %s dans l'iut de %s !\n", departement, ville);
}

/*Affiche toutes les villes qui possèdent un IUT*/

void afficherVilleIUT(void * param) {

	GenericSessionState * session = (GenericSessionState*) param;

	int i;
	printf("Voici les villes possédant un IUT : \n\n");
	for(i=0; i<session->IUTs.nbVille; i++) {
		puts(session->IUTs.iut[i]->ville);
	}
	printf("\n\n");
}

/*Affiche les IUT possédants un département spécifique*/

void trouverDepartement(void * param) {

	GenericSessionState * session = (GenericSessionState*) param;

	char departement[CHAINE];

	printf("Nom du département (vide pour annuler): ");
	int r = read_line_opt(departement, sizeof_array(departement), stdin);
	if (r == EOF || !r) return;

	int i;
	printf("Voici la liste des IUT qui possèdent un département %s : \n\n\n", departement);
	for(i=0; i<session->IUTs.nbVille; i++) {
		ListeDept temp=session->IUTs.iut[i]->ldept;
		while(temp != NULL) {
			if(strcmp(departement, temp->departement)==0) {
				puts(session->IUTs.iut[i]->ville);
				break;
			}
			temp=temp->suiv;
		}

	}
	printf("\n\n");
}
