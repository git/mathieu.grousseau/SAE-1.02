# SAE-1.02 - Comparaison d'approches algorithmiques
Un programme de gestion des candidatures des futurs étudiants.

Vous pouvez trouver la documentation [ici](Documentation.pdf).

Dépot codefist:
[https://codefirst.iut.uca.fr/git/mathieu.grousseau/SAE-1.02](https://codefirst.iut.uca.fr/git/mathieu.grousseau/SAE-1.02)

## Autheurs
 *  Cléo Eiras
 *  Mathieu Grousseau

## Compiler
Pour compiler le projet, tapez `make` dans le dossier racine du projet.

## Documentation
Pour générer la documentation, tapez `make doc` dans le dossier racine du projet.
La documentation générée est rangée dans le répertoire `doc`.

## Nettoyer
Pour nettoyer les fichier générés, tapez `make clean` dans le dossier racine du projet.